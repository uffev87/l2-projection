#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 17:03:32 2024

@author: ulf
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

A = []

with open('plot_nodes.txt') as f:
    for line in f: # read rest of lines
        A.append([float(x) for x in line.split()])

def plot(A):

    d = len(A) - 1
    n = len(A[0])

    if d == 1:

        X = np.array(A[0])
        Y = np.array(A[1])

        plt.plot(X, Y)

    elif d == 2:

        X = np.array(A[0])
        Y = np.array(A[1])
        Z = np.array(A[2]).reshape(n, n)

        img = plt.contourf(X, Y, Z, cmap = plt.viridis())
        plt.colorbar(img)

    elif d == 3:

        X = np.empty(n**3)
        Y = np.empty(n**3)
        Z = np.empty(n**3)

        l = 0
        for i in range(n):
            for j in range(n):
                for k in range(n):
                    X[l] = A[0][i]
                    Y[l] = A[1][j]
                    Z[l] = A[2][k]
                    l += 1

        U = np.array(A[3]).reshape(n, n, n)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection = '3d')
        img = ax.scatter(X, Y, Z, c = U, cmap = plt.viridis())
        plt.colorbar(img)
