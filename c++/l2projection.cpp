/*
This program approximates an arbitrary function f of dimension d
over a line, square or cube of interval [a, b]^d. This is done with
l2 projection, which means that the approximation is a polynomial
of degree p, which is a best approximation in l2-sense.
*/

#include <cmath>
#include <functional>
#include <iostream>
#include <fstream>
using namespace std;

const double tol = 1e-12; // tolerance of each approximation.

/*
enumerate is used to fetch the correct indices for a
given element for some dimension d and max length n.
*/

int* enumerate(int e, int d, int n) {

    int* index = (int*) malloc(d * sizeof(int));
    for (int i = 0; i < d; i++) {
        index[i] = int(e/pow(n, i))%n;
    }

    return index;
}

/*
Sums a function f(k) such that s = sum_{k = 0}^n f(k).
*/

double sum(function<double(double)> f, int i, int n) {

    double y = 0;
    for (int k = i; k <= n; k++) {
        y += f(k);
    }

    return y;
}

/*
sumfactorization is used to compute functional values
given some basis functions, called psi_ip0(xi_0)
and a set of nodes called uhat of dimension 1, 2 or 3.
An example of a basis function is
psi_ip0(xi_0) = P(xi_0, p0, a, b), where P is
defined as Legendre polynomial of degree p0 over [a, b].
*/

/*
Sum-factorization performed in 1D.
The functional value is computed with the formula
u(xi_0) = sum_(p0 = 0)^p psi_ip0(xi_0)uhat_p0
*/

double sumfactorization(function<double(double, int)> psi_i, \
                        double xi_0, double* uhat, int p) {

    double y = 0;
    for (int p0 = 0; p0 <= p; p0++) {
        y += psi_i(xi_0, p0) * uhat[p0];
    }

    return y;
}

/*
Sum-factorization performed in 2D.
The functional value is computed with the formula
u(xi_0, xi_1) = sum_(p0 = 0)^p psi_ip0(xi_0)
sum_(p1 = 0)^p psi_jp1(xi_1)uhat_p0p1
*/

double sumfactorization(function<double(double, int)> psi_i, \
                        function<double(double, int)> psi_j, \
                        double xi_0, double xi_1, \
                        double** uhat, int p) {

    double y = 0;
    for (int p0 = 0; p0 <= p; p0++) {
        y += psi_i(xi_0, p0) * \
        sumfactorization(psi_j, xi_1, uhat[p0], p);
    }

    return y;
}

/*
Sum-factorization performed in 3D.
The functional value is computed with the formula
u(xi_0, xi_1, xi_2) = sum_(p0 = 0)^p psi_ip0(xi_0)
sum_(p1 = 0)^p psi_jp1(xi_1) sum_(p2 = 0)^p psi_kp2(xi_3)uhat_p0p1p2
*/

double sumfactorization(function<double(double, int)> psi_i, \
                        function<double(double, int)> psi_j, \
                        function<double(double, int)> psi_k, \
                        double xi_0, double xi_1, double xi_2, \
                        double*** uhat, int p) {

    double y = 0;
    for (int p0 = 0; p0 <= p; p0++) {
        y += psi_i(xi_0, p0) * \
        sumfactorization(psi_j, psi_k, xi_1, xi_2, uhat[p0], p);
    }

    return y;
}

/*
Reshapes a vector of length n1 * n2
to a matrix of shape n1 x n2.
*/

double** reshape(double* v, int n1, int n2) {

    int k = 0;
    double** A = (double**) malloc(n1 * sizeof(double*));
    for (int i = 0; i < n1; i++) {
        A[i] = (double*) malloc(n1 * sizeof(double));
        for (int j = 0; j < n2; j++) {
            A[i][j] = v[k];
            k++;
        }
    }

    return A;
}

/*
Reshapes a vector of length n1 * n2 * n3
to a matrix of shape n1 x n2 x n3.
*/

double*** reshape(double* v, int n1, int n2, int n3) {

    int l = 0;
    double*** A = (double***) malloc(n1 * sizeof(double**));
    for (int i = 0; i < n1; i++) {
        A[i] = (double**) malloc(n1 * sizeof(double*));
        for (int j = 0; j < n2; j++) {
            A[i][j] = (double*) malloc(n2 * sizeof(double));
            for (int k = 0; k < n3; k++) {
                A[i][j][k] = v[l];
                l++;
            }
        }
    }

    return A;
}

/*
Creates n equidistant points over the interval [a, b].
*/

double* linspace(double a, double b, int n) {

    double* y = (double*) malloc(n * sizeof(double));
    for (int i = 0; i < n; i++) {
        y[i] = ((n - 1 - i) * a + i * b)/(n - 1);
    }

    return y;
}

/*
Mesh grid over the domain [a, b]^d.
The structure of the mesh grid is
such that it builds n^d sub-domains
equidistantly over the whole domain.
*/

double*** meshgrid(double a, double b, int d, int n) {

    double* points = linspace(a, b, n + 1);

    double*** grid = (double***) malloc(pow(n, d) * sizeof(double**));
    for (int e = 0; e < pow(n, d); e++) {
        grid[e] = (double**) malloc(d * sizeof(double*));
        int* index = enumerate(e, d, n);
        for (int i = 0; i < d; i++) {
            grid[e][i] = (double*) malloc(2 * sizeof(double));
            for (int j = 0; j < 2; j++) {
                grid[e][i][j] = points[index[i] + j];
            }
        }
    }

    return grid;
}

/*
grid_element finds which of the sub-domains a vector-valued
function is in and returns the corresponding element value.
*/

int grid_element(double* x, double*** grid, int d, int n) {

    int element = 0;

    for (int i = 0; i < d; i++) {
        for (int e = element; e < pow(n, i + 1); e += pow(n, i)) {
            if (x[i] >= grid[e][i][0] & x[i] <= grid[e][i][1]) {
                element = e;
                break;
            }
        }
    }

    return element;
}

/*
binomial(n, k) = n * (n - 1)/2 * ... * (n + 1 - k)/k!
*/

double binomial(double n, int k) {

    if (k == 0) {
        return 1;
    }

    if (n < 0) {
        return (1 - 2 * (k%2)) * binomial(k - 1 - n, k);
    }

    double y = 1;
    for (double i = 1; i <= k; i++) {
        y *= (n + 1 - i)/i;
        if (y == 0) {
            break;
        }
    }

    return y;
}


/*
Legendre polynomial of degree n over the interval [a, b].
Computed with Rodrigues formula P_n(xtilde) = sqrt(2n + 1)2^n
sum_(k = 0)^n(xtilde^k)binomial(n, k)binomial((n + k - 1)/2, n),
where xtilde is the shift (2x - (a + b))/(b - a).
*/

double P(double x, int p, double a, double b) {

    double xtilde = (2 * x - a - b)/(b - a);

    auto g = [&](auto k) {return pow(xtilde, k) * \
    binomial(p, k) * binomial(0.5 * (p + k - 1), p);};

    double y = sqrt(2 * p + 1) * pow(2, p) * sum(g, 0, p);

    return y;
}

/*
Derivative of Legendre polynomial, i.e. d(P(x, n, a, b))/dx.
*/

double dP(double x, int p, double a, double b) {

    double xtilde = (2 * x - a - b)/(b - a);

    auto g = [&](auto k) {return (k + 1) * pow(xtilde, k) * \
    binomial(p, k + 1) * binomial(0.5 * (p + k), p);};

    double y = sqrt(2 * p + 1) * pow(2, p + 1) * sum(g, 0, p - 1)/(b - a);

    return y;
}

/*
Iteratively approximates the solution of some function
f(x) = 0, with Newton's method
x_(k + 1) = x_k - f(x_k)/df(x_k),
where df(x) is the derivative of f(x).
*/

double Newton(function<double(double)> f, \
              function<double(double)> df, \
              double x, double tol) {

    double x_new = x - f(x)/df(x);

    while (abs(x_new - x) > tol) {
        x = x_new;
        x_new = x - f(x)/df(x);
    }

    return x_new;
}

/*
Creates quadrature points x_i over [a, b],
where 0 <= i <= p.
The quadrature points are the zeros
of the Legendre polynomial L(x, p, a, b).
*/

double* Legendre_quad_points(double a, double b, int p, double tol) {

    auto f = [&](auto x) {return P(x, p + 1, a, b);};
    auto df = [&](auto x) {return dP(x, p + 1, a, b);};

    double c = p/(16 * pow(p + 1, 3)) - 0.5;
    double d = p + 1.5;

    double h = b - a;

    double* x = (double*) malloc((p + 1) * sizeof(double));
    for (int i = 0; i <= p; i++) {
        double x0 = a + h * (0.5 + c * cos((i + 0.75) * M_PI/d));
        x[i] = Newton(f, df, x0, tol);
    }

    return x;
}

/*
One weight point of a corresponding quadrature point.
The quadrature point is a zero of the Legendre polynomial P(x, p, a, b).
*/

double Legendre_weight_point(double x, double a, double b, int p) {

    double k = (8 * p + 20)/((b - a) * pow(p + 2, 2));

    double w = k * (x - a) * (b - x)/pow(P(x, p + 2, a, b), 2);

    return w;
}

/*
Weight points of the corresponding quadrature points.
The quadrature points are the zeros of the Legendre polynomial P(x, p, a, b).
*/

double* Legendre_weight_points(double* x, double a, double b, int p) {

    double k = (8 * p + 20)/((b - a) * pow(p + 2, 2));

    double* w = (double*) malloc((p + 1) * sizeof(double));
    for (int i = 0; i <= p; i++) {
        w[i] = k * (x[i] - a) * (b - x[i])/pow(P(x[i], p + 2, a, b), 2);
    }

    return w;
}

/*
quad is used to compute numerical integration
of functions over the domain [a, b].
It does so by evaluating quadrature points x_i
and weight points w_i, where 0 <= i <= p.
The points x_i and w_i can also be used when
a function is being approximated by l2 projection.
*/

double quad(function<double(double*, int)> f, \
            double*** grid, int d, int p, int n, double tol) {

    double* x_base = Legendre_quad_points(0, 1, p, tol);

    int* index = (int*) malloc(d * sizeof(int));
    int* index_old = (int*) malloc(d * sizeof(int));
    for (int i = 0; i < d; i++) {
        index_old[i] = -1;
    }

    double* quad_pts = (double*) malloc(d * sizeof(double));
    double* w = (double*) malloc(d * sizeof(double));

    double I = 0;

    for (int e = 0; e < pow(n, d); e++) {

        for (int i = 0; i < pow(p + 1, d); i++) {
            index = enumerate(i, d, p + 1);
            for (int j = 0; j < d; j++) {
                double a = grid[e][j][0];
                double b = grid[e][j][1];
                double h = b - a;
                if (index[j] != index_old[j]) {
                    quad_pts[j] = a + h * x_base[index[j]];
                    w[j] = Legendre_weight_point(quad_pts[j], a, b, p);
                }
            }

            index_old = index;

            double f_val = f(quad_pts, d);

            if (d == 1) {

                for (int p0 = 0; p0 <= p; p0++) {
                    I += w[p0] * f_val;
                }
            }

            else if (d == 2) {

                for (int p0 = 0; p0 <= p; p0++) {
                    double f_0 = w[p0] * f_val;
                    for (int p1 = 0; p1 <= p; p1++) {
                        I += w[p1] * f_0;
                    }
                }
            }

            else if (d == 3) {

                for (int p0 = 0; p0 <= p; p0++) {
                    double f_0 = w[p0] * f_val;
                    for (int p1 = 0; p1 <= p; p1++) {
                        double f_01 = w[p1] * f_0;
                        for (int p2 = 0; p2 <= p; p2++) {
                            I += w[p2] * f_01;
                        }
                    }
                }
            }
        }
    }

    return I;
}

/*
Basis functions psi_{iq} = (w_i)P(x_i, q, 0, 1),
which are used for l2 projection.
*/

double** psi_vals(double* x, double* w, int p) {

    double** psi = (double**) malloc((p + 1) * sizeof(double*));
    for (int i = 0; i <= p; i++) {
        psi[i] = (double*) malloc((p + 1) * sizeof(double));
        for (int q = 0; q <= p; q++) {
            psi[i][q] = w[i] * P(x[i], q, 0, 1);
        }
    }

    return psi;
}

/*
Performs l2 projection of a mesh grid of shape n^d
to some function f. It will compute the constants uhat
that will be used to create a polynomial approximation of f.
*/

double** uhat_vals(function<double(double*, int)> f, \
                   double*** grid, int d, int p, int n) {

    double* x_base = Legendre_quad_points(0, 1, p, tol);
    double* w_base = Legendre_weight_points(x_base, 0, 1, p);

    double** psi = psi_vals(x_base, w_base, p);

    int* index = (int*) malloc(d * sizeof(int));
    int* index_old = (int*) malloc(d * sizeof(int));
    for (int i = 0; i < d; i++) {
        index_old[i] = -1;
    }

    double* quad_pts = (double*) malloc(d * sizeof(double));

    double** uhat = (double**) malloc(pow(n, d) * sizeof(double*));
    for (int e = 0; e < pow(n, d); e++) {
        uhat[e] = (double*) malloc(pow(p + 1, d) * sizeof(double));
        for (int q = 0; q < pow(p + 1, d); q++) {
            uhat[e][q] = 0;
        }
    }

    for (int e = 0; e < pow(n, d); e++) {

        for (int i = 0; i < pow(p + 1, d); i++) {
            index = enumerate(i, d, p + 1);
            for (int j = 0; j < d; j++) {
                double a = grid[e][j][0];
                double b = grid[e][j][1];
                double h = b - a;
                if (index[j] != index_old[j]) {
                    quad_pts[j] = a + h * x_base[index[j]];
                }
            }

            index_old = index;

            double f_val = f(quad_pts, d);

            int q = 0;

            if (d == 1) {

                for (int p0 = 0; p0 <= p; p0++) {
                    uhat[e][p0] += psi[index[0]][p0] * f_val;
                }
            }

            else if (d == 2) {

                for (int p0 = 0; p0 <= p; p0++) {
                    double f_0 = psi[index[0]][p0] * f_val;
                    for (int p1 = 0; p1 <= p; p1++) {
                        uhat[e][q] += psi[index[1]][p1] * f_0;
                        q++;
                    }
                }
            }

            else if (d == 3) {

                for (int p0 = 0; p0 <= p; p0++) {
                    double f_0 = psi[index[0]][p0] * f_val;
                    for (int p1 = 0; p1 <= p; p1++) {
                        double f_01 = psi[index[1]][p1] * f_0;
                        for (int p2 = 0; p2 <= p; p2++) {
                            uhat[e][q] += psi[index[2]][p2] * f_01;
                            q++;
                        }
                    }
                }
            }
        }
    }

    return uhat;
}

/*
Computes the functional approximation of f of dimension d
by the polynomial spline function u of degree p, that uses
the previously computed values of uhat and defined grid.
*/

double u(double* xi, double** uhat, double*** grid, int d, int p, int n) {

    double y;

    int e = grid_element(xi, grid, d, n);

    double* a = (double*) malloc(d * sizeof(double));
    double* b = (double*) malloc(d * sizeof(double));
    for (int i = 0; i < d; i++) {
        a[i] = grid[e][i][0];
        b[i] = grid[e][i][1];
    }

    if (d == 1) {
        auto psi_i = [&](double x, int p0) {return P(x, p0, a[0], b[0]);};

        double* uhat_e = uhat[e];

        y = sumfactorization(psi_i, xi[0], uhat_e, p);
    }

    else if (d == 2) {
        auto psi_i = [&](double x, int p0) {return P(x, p0, a[0], b[0]);};
        auto psi_j = [&](double x, int p1) {return P(x, p1, a[1], b[1]);};

        double** uhat_e = reshape(uhat[e], p + 1, p + 1);

        y = sumfactorization(psi_i, psi_j, xi[0], xi[1], uhat_e, p);
    }

    else if (d == 3) {
        auto psi_i = [&](double x, int p0) {return P(x, p0, a[0], b[0]);};
        auto psi_j = [&](double x, int p1) {return P(x, p1, a[1], b[1]);};
        auto psi_k = [&](double x, int p2) {return P(x, p2, a[2], b[2]);};

        double*** uhat_e = reshape(uhat[e], p + 1, p + 1, p + 1);

        y = sumfactorization(psi_i, psi_j, psi_k, \
                             xi[0], xi[1], xi[2], uhat_e, p);
    }

    return y;
}

double** plot_nodes(function<double(double*, int)> f, \
                    double a, double b, int d, int p, int n) {

    int m = (p + 1) * n + 1;
    int m_pow = pow(m, d);

    double** nodes = (double**) malloc((d + 1) * sizeof(double*));
    for (int i = 0; i < d; i++) {
        nodes[i] = linspace(a, b, m);
    }

    nodes[d] = (double*) malloc(m_pow * sizeof(double));
    for (int e = 0; e < m_pow; e++) {
        int* index = enumerate(e, d, m);
        double* coords = (double*) malloc(d * sizeof(double));
        for (int i = 0; i < d; i++) {
            coords[i] = nodes[i][index[i]];
        }
        nodes[d][e] = f(coords, d);
    }

    return nodes;
}

int main() {

    double a, b;
    int d, p, n;

    string integrate;

    cout << "Left boundary point of the space: ";
    cin >> a;

    cout << "Right boundary point of the space: ";
    cin >> b;

    cout << "Dimension of the function: ";
    cin >> d;

    cout << "Polynomial degree of the approximation: ";
    cin >> p;

    cout << "Number of grids from " << a << " to " << b << ": ";
    cin >> n;

    double*** grid = meshgrid(a, b, d, n);

    // f is the function which is approximated with l² projection.

    auto f = [&](double* x, int d) {double y; if (d == 1) y = sin(M_PI * x[0]);
    else if (d == 2) y = sin(M_PI * x[0]) * sin(M_PI * x[1]);
    else if (d == 3) y = sin(M_PI * x[0]) * sin(M_PI * x[1]) * sin(M_PI * x[2]);
    return y;};

    // Nodes used to compute the approximated polynomial of f.

    double** uhat = uhat_vals(f, grid, d, p, n);

    // Approximated polynomial of f.

    auto uh = [&](double* x, int d) {return u(x, uhat, grid, d, p, n);};

    // Creates values that enables plotting.

    double** nodes = plot_nodes(uh, a, b, d, p, n);

    ofstream myfile("plot_nodes.txt");
    if (myfile.is_open()) {

        int m = (p + 1) * n + 1;

        for (int i = 0; i < d; i++) {
            for (int j = 0; j < m; j++) {
                myfile << nodes[i][j] << " ";
            }
            myfile << endl;
        }

        for (int e = 0; e < pow(m, d); e++) {
            myfile << nodes[d][e] << " ";
        }
        myfile << endl;

        myfile.close();
    }

    else {
        cout << "Unable to open file.";
    }

    cout << endl;
    cout << "Type y or yes if you want to compute the l2-error of uh: ";
    cin >> integrate;

    // Changes string characters to lowercase.

    for (auto& c:integrate) {
        c = tolower(c);
    }

    if (integrate == "y" or integrate == "yes") {

        // Error defined as (uh - f)².

        auto E = [&](double* x, int d) {return pow(uh(x, d) - f(x, d), 2);};

        // This computes the l2 error of the approximated polynomial.

        double I = sqrt(quad(E, grid, d, p + 1, n, tol));

        cout << endl;
        cout << "The l2-error of uh is " << I << ". The integration error is of order "
        << 2 * p + 3 << " and is summed of " << pow(n, d) <<
        " equidistant subdomains over the domain [" << a << ", " << b << "]^" << d << endl;
    }

    return 0;
}
